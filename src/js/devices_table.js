$.tabulate = function (data) {
    var table = d3
        .select("#csvtable")
        .append("table")
        .attr("id", "datatable")
        .attr("class", "table table-bordered table-hover dt-responsive nowrap")
        .attr("cellspacing", 0)
        .attr("style", "width:100%;border-collapse:collapse;")
        .attr("width", "100%");
    var thead = table.append("thead");
    var tbody = table.append("tbody");

    thead
        .append("tr")
        .selectAll(null)
        .data(data.shift())
        .enter()
        .append("th")
        .text((d) => d);

    var rows = tbody.selectAll(null).data(data).enter().append("tr");

    rows
        .selectAll(null)
        .data((d) => d)
        .enter()
        .append("td")
        .text((d) => d);

    var searchTerm = getUrlVars()['search'];
    $("#datatable").DataTable({
        search: { search: searchTerm },
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search..."
        },
        lengthMenu: [
            [20, 40, 60, 100, 150, 200, -1],
            [20, 40, 60, 100, 150, 200, "All"]
        ],
        "drawCallback": function( settings ) {
            //Force datatable to recalculate the column size when redraw, be sure it won't overflow
            $('#datatable').DataTable()
            .columns.adjust()
            .responsive.recalc();
        },
    });

    $('#datatable').on('search.dt', function() {
        var value = $('.dataTables_filter input').val();
        updateQueryString('search', value);
    });

    return table;
}

$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
    var selectedItem = $("#cmbCategory").val();
    var category = data[1];
    if (selectedItem === "" || category.includes(selectedItem)) {
        return true;
    }
    return false;
});

//Fix the pagination on mobile devices
$.fn.DataTable.ext.pager.numbers_length = screen.width <= 480? 5: 7;

function getUrlVars() {
    var url = window.location.href,
        vars = {};
    url.replace(/\+/g,'%20').replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
         key = decodeURIComponent(key);
         value = decodeURIComponent(value);
         vars[key] = value;
    });
    return vars;
}

function updateQueryString(key, value) {
    if (history.pushState) {
        let searchParams = new URLSearchParams(window.location.search);
        searchParams.set(key, value);
        let newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + searchParams.toString();
        window.history.pushState({path: newurl}, '', newurl);
    }
}